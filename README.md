# ScreenKey Configuration

ScreenKey configuration (fizzy compliant)

## Documentation

See [wiki](https://gitlab.com/alem0lars/configs-screenkey/wikis/home)

## Contributing

See [CONTRIBUTING](./CONTRIBUTING.md)

## License

See [LICENSE](./LICENSE)